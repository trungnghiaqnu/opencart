<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* catalog/view/template/product/thumb.twig */
class __TwigTemplate_8db70610e93ad2fc2bc046dde945938fe19ebad7d325ef84e50b1cbea4abee44 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<form method=\"post\" data-oc-toggle=\"ajax\" data-oc-load=\"";
        echo ($context["cart"] ?? null);
        echo "\" data-oc-target=\"#header-cart\">
  <div class=\"product-thumb\">
    <div class=\"image customs\">
      <a><img src=\"";
        // line 4
        echo ($context["thumb"] ?? null);
        echo "\" alt=\"";
        echo ($context["name"] ?? null);
        echo "\" title=\"";
        echo ($context["name"] ?? null);
        echo "\" class=\"img-fluid\"/></a>
      <div class=\"product-quick-view\">
          <a href=\"";
        // line 6
        echo ($context["href"] ?? null);
        echo "\" class=\"btn-product-quick-view\" href=\"javascript:void(0)\" title=\"Xem nhanh ";
        echo ($context["name"] ?? null);
        echo "\"></a>
      </div>
      <div class=\"btn-quick-cart\">
          <button type=\"submit\" class=\"btn-add-to-cart\" formaction=\"";
        // line 9
        echo ($context["add_to_cart"] ?? null);
        echo "\" title=\"";
        echo ($context["button_cart"] ?? null);
        echo "\"><i class=\"fa-solid fa-shopping-cart\"></i></button>
      </div>
    </div>
  </div>
  <div class=\"content\">
    <div class=\"description\">
      <h4><a class\"title-product\" href=\"";
        // line 15
        echo ($context["href"] ?? null);
        echo "\">";
        echo ($context["name"] ?? null);
        echo "</a></h4>
      ";
        // line 17
        echo "      ";
        if (($context["price"] ?? null)) {
            // line 18
            echo "        <div class=\"price\">
          ";
            // line 19
            if ( !($context["special"] ?? null)) {
                // line 20
                echo "            <span class=\"price-new\">";
                echo ($context["price"] ?? null);
                echo "</span>
          ";
            } else {
                // line 22
                echo "            <span class=\"price-new\">";
                echo ($context["special"] ?? null);
                echo "</span> <span class=\"price-old\">";
                echo ($context["price"] ?? null);
                echo "</span>
          ";
            }
            // line 24
            echo "          ";
            if (($context["tax"] ?? null)) {
                // line 25
                echo "            <span class=\"price-tax\">";
                echo ($context["text_tax"] ?? null);
                echo " ";
                echo ($context["tax"] ?? null);
                echo "</span>
          ";
            }
            // line 27
            echo "        </div>
      ";
        }
        // line 29
        echo "      ";
        if ((($context["review_status"] ?? null) && ($context["rating"] ?? null))) {
            // line 30
            echo "        <div class=\"rating\">
          ";
            // line 31
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 5));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 32
                echo "            ";
                if ((($context["rating"] ?? null) < $context["i"])) {
                    // line 33
                    echo "              <span class=\"fa-stack\"><i class=\"fa-regular fa-star fa-stack-1x\"></i></span>
            ";
                } else {
                    // line 35
                    echo "              <span class=\"fa-stack\"><i class=\"fa-solid fa-star fa-stack-1x\"></i><i class=\"fa-regular fa-star fa-stack-1x\"></i></span>
            ";
                }
                // line 37
                echo "          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 38
            echo "        </div>
      ";
        }
        // line 40
        echo "    
      <div class=\"button-group btn-cart-product col-md-show col-lg-hidden\">
        <button type=\"submit\" formaction=\"";
        // line 42
        echo ($context["add_to_cart"] ?? null);
        echo "\" data-bs-toggle=\"tooltip\" title=\"";
        echo ($context["button_cart"] ?? null);
        echo "\" class=\"btn btn-outline-primary w-100\"><i class=\"fa-solid fa-shopping-cart\"></i></button>
      </div>
    </div>
    <input type=\"hidden\" name=\"product_id\" value=\"";
        // line 45
        echo ($context["product_id"] ?? null);
        echo "\"/>
    <input type=\"hidden\" name=\"quantity\" value=\"";
        // line 46
        echo ($context["minimum"] ?? null);
        echo "\"/>
  </div>
</form>
";
    }

    public function getTemplateName()
    {
        return "catalog/view/template/product/thumb.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 46,  158 => 45,  150 => 42,  146 => 40,  142 => 38,  136 => 37,  132 => 35,  128 => 33,  125 => 32,  121 => 31,  118 => 30,  115 => 29,  111 => 27,  103 => 25,  100 => 24,  92 => 22,  86 => 20,  84 => 19,  81 => 18,  78 => 17,  72 => 15,  61 => 9,  53 => 6,  44 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "catalog/view/template/product/thumb.twig", "C:\\xampp\\htdocs\\opencart\\upload\\catalog\\view\\template\\product\\thumb.twig");
    }
}
