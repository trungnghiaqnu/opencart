<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* catalog/view/template/common/search.twig */
class __TwigTemplate_79c7895c35ef8906cc9ac3dc7cc78492c6d755b299e152d7e0a85c30062a1366 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div id=\"search\" class=\"input-group mb-0 pd5\">
\t<div class=\"site-nav-container\">
\t\t<p class=\"title-box\">";
        // line 3
        echo ($context["text_search"] ?? null);
        echo "</p>
\t\t<div class=\"mt-3 search-form\">
\t\t\t<input value=\"";
        // line 5
        echo ($context["search"] ?? null);
        echo "\" name=\"search\" maxlength=\"40\" class=\"form-control search-input\" type=\"text\" size=\"20\" placeholder=\"...\">
\t\t\t<button type=\"button\" class=\"btn btn-search\"  data-lang=\"";
        // line 6
        echo ($context["language"] ?? null);
        echo "\"><i class=\"fas fa-search\"></i></button>
\t\t</div>
\t</div>
\t";
        // line 11
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "catalog/view/template/common/search.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 11,  50 => 6,  46 => 5,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "catalog/view/template/common/search.twig", "C:\\xampp\\htdocs\\opencart\\upload\\catalog\\view\\template\\common\\search.twig");
    }
}
